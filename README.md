# Block-Journey
Source code for learning blockchain development

## Repo list:
- [Create simple ERC20 & BEP20 Tokens](https://github.com/santheipman/Create-simple-ERC20-BEP20-Tokens)
- [Write tests for smart contract | Hardhat](https://github.com/santheipman/hardhat-test)
